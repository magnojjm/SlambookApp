package com.wesleyan.app.slambook.slambookapp;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Slams_Schema implements Serializable {

    @SerializedName("ID")
    public String ID;
    @SerializedName("fname")
    public String fname;
    @SerializedName("lname")
    public String lname;
    @SerializedName("birthday")
    public String birthday;
    @SerializedName("email")
    public String email;
    @SerializedName("age")
    public String age;
    @SerializedName("zodiac")
    public String zodiac;
    @SerializedName("hobby")
    public String hobby;
    @SerializedName("ambition")
    public String ambition;
    @SerializedName("love")
    public String love;
    @SerializedName("fcrush")
    public String fcrush;
    @SerializedName("flove")
    public String flove;
    @SerializedName("path")
    public String path;
}
