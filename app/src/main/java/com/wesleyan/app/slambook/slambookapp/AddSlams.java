package com.wesleyan.app.slambook.slambookapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddSlams extends AppCompatActivity {
    Spinner gender;
    ImageView ivProfile;
    Button btnAddPic,btnSubmit;
    EditText edFname,edLname,edBirthday,edEmail,edAge, edZodiac, edHobby, edAmbition,edLove,edFcrush,edFlove;
    GalleryPhoto galleryPhoto;
    final int GALLERY_REQUEST = 2200;
    String selectedPhoto;
    ProgressDialog dialog12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_slams);

        dialog12 = new ProgressDialog(AddSlams.this);
        ivProfile = (ImageView)findViewById(R.id.ivPicture);
        btnAddPic = (Button)findViewById(R.id.btnAddPic);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        edFname = (EditText)findViewById(R.id.fname);
        edLname  = (EditText)findViewById(R.id.lname);
        edBirthday  = (EditText)findViewById(R.id.birthday);
        edEmail  = (EditText)findViewById(R.id.email);
        edAge = (EditText)findViewById(R.id.age);
        edZodiac  = (EditText)findViewById(R.id.zodiac);
        edHobby  = (EditText)findViewById(R.id.hobby);
        edAmbition = (EditText)findViewById(R.id.ambition);
        edLove  = (EditText)findViewById(R.id.love);
        edFcrush  = (EditText)findViewById(R.id.fcrush);
        edFlove  = (EditText)findViewById(R.id.flove);
        galleryPhoto = new GalleryPhoto(getApplicationContext());
        btnSubmit.setEnabled(false);
        btnAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
             public void onClick(View view) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edFname.getText().toString().equals("") ||
                        edLname.getText().toString().equals("") ||
                        edBirthday.getText().toString().equals("") ||
                        edEmail.getText().toString().equals("") ||
                        edAge.getText().toString().equals("") ||
                        edZodiac.getText().toString().equals("") ||
                        edHobby.getText().toString().equals("") ||
                        edAmbition.getText().toString().equals("") ||
                        edLove.getText().toString().equals("") ||
                        edFcrush.getText().toString().equals("") ||
                        edFlove.getText().toString().equals("")){
                    INC();
                }else {
                    try {
                        Bitmap bitmap = ImageLoader.init().from(selectedPhoto).requestSize(150, 150).getBitmap();
                        final String encodeIMG = ImageBase64.encode(bitmap);


                        String url = "https://slambookapp.000webhostapp.com/insert.php";

                        StringRequest addalyrics = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                dialog12.dismiss();
                                if (response.equals("Try Again")) {
                                    Toast.makeText(AddSlams.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(AddSlams.this, "Slam Successfully Created", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), Home.class);
                                    startActivity(intent);
                                    AddSlams.this.finish();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                dialog12.dismiss();
                                if (error instanceof TimeoutError) {
                                    Timeout();
                                } else if (error instanceof NoConnectionError) {
                                    offlineMODE();
                                }
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> postData = new HashMap<>();
                                postData.put("fname", edFname.getText().toString());
                                postData.put("lname", edLname.getText().toString());
                                postData.put("birthday", edBirthday.getText().toString());
                                postData.put("email", edEmail.getText().toString());
                                postData.put("age", edAge.getText().toString());
                                postData.put("zodiac", edZodiac.getText().toString());
                                postData.put("hobby", edHobby.getText().toString());
                                postData.put("ambition", edAmbition.getText().toString());
                                postData.put("love", edLove.getText().toString());
                                postData.put("fcrush", edFcrush.getText().toString());
                                postData.put("flove", edFlove.getText().toString());
                                postData.put("image", encodeIMG);
                                return postData;
                            }
                        };
                        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(addalyrics);
                        dialog12.setTitle("Inserting Slam");
                        dialog12.setCancelable(false);
                        dialog12.setMessage("Please Wait");
                        dialog12.show();
                    } catch (FileNotFoundException e) {
                        Toast.makeText(AddSlams.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });


    }


    @NonNull
    private ArrayList<String> addGender() {
        ArrayList<String> gender_array = new ArrayList<>();
        gender_array.add("Male");
        gender_array.add("Female");
        gender_array.add("Others");
        return gender_array;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode == GALLERY_REQUEST){
                galleryPhoto.setPhotoUri(data.getData());
                String photoPath = galleryPhoto.getPath();
                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(150, 150).getBitmap();
                    ivProfile.setImageBitmap(bitmap);
                    selectedPhoto = photoPath;
                    btnSubmit.setEnabled(true);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private void offlineMODE(){
        final AlertDialog alertDialog = new AlertDialog.Builder(AddSlams.this).create();
        alertDialog.setTitle("Offline");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
    private void Timeout(){
        final AlertDialog alertDialog = new AlertDialog.Builder(AddSlams.this).create();
        alertDialog.setTitle("Connection Timeout");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
    private void INC(){
        final AlertDialog alertDialog = new AlertDialog.Builder(AddSlams.this).create();
        alertDialog.setTitle("Incomplete");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please Complete the Requirements");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
}
