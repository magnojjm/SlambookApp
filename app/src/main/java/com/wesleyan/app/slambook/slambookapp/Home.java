package com.wesleyan.app.slambook.slambookapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kosalgeek.android.json.JsonConverter;
import com.wesleyan.app.slambook.slambookapp.adapters.PlaceAdapter;

import java.io.Serializable;
import java.util.ArrayList;

public class Home extends AppCompatActivity implements Serializable {
    RecyclerView recyclerView;
    PlaceAdapter adapter1;
    ProgressDialog dialog12;
    ArrayList<String> sid,sfname,slname,sprofile, sbirthday, semail, sage, szodiac, shobby, sambition, slove, sfcrush, sflove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        addSlams();

        recyclerView = (RecyclerView)findViewById(R.id.rv_place);
        dialog12 = new ProgressDialog(Home.this);

        sprofile = new ArrayList<String>();
        sfname = new ArrayList<String>();
        slname = new ArrayList<String>();
        sid = new ArrayList<String>();
        sbirthday = new ArrayList<String>();
        semail = new ArrayList<String>();
        sage = new ArrayList<String>();
        szodiac = new ArrayList<String>();
        shobby = new ArrayList<String>();
        sambition = new ArrayList<String>();
        slove = new ArrayList<String>();
        sfcrush = new ArrayList<String>();
        sflove = new ArrayList<String>();

        String url = "https://slambookapp.000webhostapp.com/retrieve.php";

        StringRequest showslams = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("No Results Found.")) {
                    notMatch();
                } else {
                    ArrayList<Slams_Schema> slams_info = new JsonConverter<Slams_Schema>().toArrayList(response, Slams_Schema.class);
                    for (Slams_Schema value : slams_info) {
                        sprofile.add(value.path);
                        sfname.add(value.fname);
                        slname.add(value.lname);
                        sbirthday.add(value.birthday);
                        semail.add(value.email);
                        sage.add(value.age);
                        szodiac.add(value.zodiac);
                        shobby.add(value.hobby);
                        sambition.add(value.ambition);
                        slove.add(value.love);
                        sfcrush.add(value.fcrush);
                        sflove.add(value.flove);
                        sid.add(value.ID);
                    }

                    System.out.println((sbirthday));
                    System.out.println((semail));
                    adapter1 = new PlaceAdapter(Home.this,sfname,slname,sprofile, sbirthday, semail, sage, szodiac, shobby, sambition, slove, sfcrush, sflove,sid);
                    recyclerView.setAdapter(adapter1);
                    recyclerView.setLayoutManager(new GridLayoutManager(Home.this, 2));
                }dialog12.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog12.dismiss();
                if (error instanceof TimeoutError){
                    Timeout();
                }else if (error instanceof NoConnectionError){
                    offlineMODE();
                }
            }
        });
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(showslams);
        dialog12.setTitle("Loading Slam");
        dialog12.setCancelable(false);
        dialog12.setMessage("Please Wait");
        dialog12.show();
    }



    private void addSlams() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this,AddSlams.class);
                startActivity(intent);
            }
        });
    }

    private void offlineMODE(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
        alertDialog.setTitle("Offline");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    private void Timeout(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
        alertDialog.setTitle("Connection Timeout");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    private void notMatch(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
        alertDialog.setTitle("No Result");
        alertDialog.setMessage("No Slams in Database");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
