package com.wesleyan.app.slambook.slambookapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wesleyan.app.slambook.slambookapp.Info;
import com.wesleyan.app.slambook.slambookapp.R;

import java.io.Serializable;
import java.util.ArrayList;


public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.PlaceViewHolder> implements Serializable {
    private Context context;
    private ArrayList<String> data_fnames;
    private ArrayList<String> data_lnames;
    private ArrayList<String> data_images;
    private ArrayList<String> data_birthday;
    private ArrayList<String> data_email;
    private ArrayList<String> data_age;
    private ArrayList<String> data_zodiac;
    private ArrayList<String> data_hobby;
    private ArrayList<String> data_ambition;
    private ArrayList<String> data_love;
    private ArrayList<String> data_fcrush;
    private ArrayList<String> data_flove;
    private ArrayList<String> id;

    public PlaceAdapter(Context context, ArrayList<String> data_fnames, ArrayList<String> data_lnames, ArrayList<String> data_images, ArrayList<String> data_birthday, ArrayList<String> data_email, ArrayList<String> data_age, ArrayList<String> data_zodiac, ArrayList<String> data_hobby, ArrayList<String> data_ambition, ArrayList<String> data_love, ArrayList<String> data_fcrush, ArrayList<String> data_flove, ArrayList<String> id) {
        this.context = context;
        this.data_fnames = data_fnames;
        this.data_lnames = data_lnames;
        this.data_images = data_images;
        this.data_birthday = data_birthday;
        this.data_email = data_email;
        this.data_age = data_age;
        this.data_zodiac = data_zodiac;
        this.data_hobby = data_hobby;
        this.data_ambition = data_ambition;
        this.data_love = data_love;
        this.data_fcrush = data_fcrush;
        this.data_flove = data_flove;
        this.id = id;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View placeView =  inflater.inflate(R.layout.item, parent, false);
        return new PlaceViewHolder(placeView);
    }

    @Override
    public void onBindViewHolder(final PlaceViewHolder holder, final int position) {

        final String fnames = data_fnames.get(position);
        final String lnames = data_lnames.get(position);
        final String images = data_images.get(position);
        final String birthday = data_birthday.get(position);
        final String email = data_email.get(position);
        final String age = data_age.get(position);
        final String zodiac = data_zodiac.get(position);
        final String hobby = data_hobby.get(position);
        final String ambition = data_ambition.get(position);
        final String love = data_love.get(position);
        final String fcrush = data_fcrush.get(position);
        final String flove = data_flove.get(position);
        final String ID = id.get(position);

        holder.tvId.setText(ID);
        holder.tvFname.setText(fnames);
        holder.tvLname.setText(lnames);
        Picasso.with(context).load(images).resize(130, 130).centerCrop().into(holder.ivProfile);

        holder.tvFname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                System.out.println(birthday);
                System.out.println(email);
                Intent next = new Intent(context, Info.class);
                next.putExtra("id", ID);
                next.putExtra("fnames", fnames);
                next.putExtra("lnames", lnames);
                next.putExtra("images", images);
                next.putExtra("birthdays", birthday);
                next.putExtra("emails", email);
                next.putExtra("ages", age);
                next.putExtra("zodiacs", zodiac);
                next.putExtra("hobbys", hobby);
                next.putExtra("ambitions", ambition);
                next.putExtra("loves", love );
                next.putExtra("fcrushs", fcrush);
                next.putExtra("floves", flove);
                context.startActivity(next);
            }
        });
        holder.tvLname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(context, Info.class);
                next.putExtra("id", ID);
                next.putExtra("fnames", fnames);
                next.putExtra("lnames", lnames);
                next.putExtra("images", images);
                next.putExtra("birthdays", birthday);
                next.putExtra("emails", email);
                next.putExtra("ages", age);
                next.putExtra("zodiacs", zodiac);
                next.putExtra("hobbys", hobby);
                next.putExtra("ambitions", ambition);
                next.putExtra("loves", love );
                next.putExtra("fcrushs", fcrush);
                next.putExtra("floves", flove);
                context.startActivity(next);
            }
        });
        holder.ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(context, Info.class);
                next.putExtra("id", ID);
                next.putExtra("fnames", fnames);
                next.putExtra("lnames", lnames);
                next.putExtra("images", images);
                next.putExtra("birthdays", birthday);
                next.putExtra("emails", email);
                next.putExtra("ages", age);
                next.putExtra("zodiacs", zodiac);
                next.putExtra("hobbys", hobby);
                next.putExtra("ambitions", ambition);
                next.putExtra("loves", love );
                next.putExtra("fcrushs", fcrush);
                next.putExtra("floves", flove);
                context.startActivity(next);
            }
        });

    }

    @Override
    public int getItemCount() {
        return id.size();
    }


    public class PlaceViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProfile;
        TextView tvFname,tvLname,tvId;
        public PlaceViewHolder(View itemView) {
            super(itemView);
            ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);
            tvFname = (TextView) itemView.findViewById(R.id.fname);
            tvLname = (TextView) itemView.findViewById(R.id.lname);
            tvId = (TextView) itemView.findViewById(R.id.tv_id);
        }
    }
}
