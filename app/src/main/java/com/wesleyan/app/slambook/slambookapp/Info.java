package com.wesleyan.app.slambook.slambookapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Info extends AppCompatActivity implements Serializable {

    ImageView ivProfile;
    Button btnDel;
    EditText edFname,edLname,edBirthday,edEmail,edAge, edZodiac, edHobby, edAmbition,edLove,edFcrush,edFlove;
    String ID, strGender;
    Bundle bundle;
    ProgressDialog dialog12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        dialog12 = new ProgressDialog(Info.this);
        ivProfile = (ImageView)findViewById(R.id.ivPicture1);
        btnDel = (Button) findViewById(R.id.btnDelete);
        edFname = (EditText)findViewById(R.id.fname1);
        edLname  = (EditText)findViewById(R.id.lname1);
        edBirthday  = (EditText)findViewById(R.id.birthday1);
        edEmail  = (EditText)findViewById(R.id.email1);
        edAge = (EditText)findViewById(R.id.age1);
        edZodiac  = (EditText)findViewById(R.id.zodiac1);
        edHobby  = (EditText)findViewById(R.id.hobby1);
        edAmbition = (EditText)findViewById(R.id.ambition1);
        edLove  = (EditText)findViewById(R.id.love1);
        edFcrush  = (EditText)findViewById(R.id.fcrush1);
        edFlove  = (EditText)findViewById(R.id.flove1);


        bundle = getIntent().getExtras();

        if(getIntent().getExtras() != null) {

            System.out.println(bundle.getString("birthdays"));
            ID = bundle.getString("id");
            strGender = bundle.getString("id");
            Picasso.with(Info.this).load(bundle.getString("images")).resize(130, 130).centerCrop().into(ivProfile);
            edFname.setText(bundle.getString("fnames"));
            edLname.setText(bundle.getString("lnames"));
            edBirthday.setText(bundle.getString("birthdays"));
            edEmail.setText(bundle.getString("emails"));
            edAge.setText(bundle.getString("ages"));
            edZodiac.setText(bundle.getString("zodiacs"));
            edHobby.setText(bundle.getString("hobbys"));
            edAmbition.setText(bundle.getString("ambitions"));
            edLove.setText(bundle.getString("loves"));
            edFcrush.setText(bundle.getString("fcrushs"));
            edFlove.setText(bundle.getString("floves"));
        }
        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://slambookapp.000webhostapp.com/delete.php";

                StringRequest delslams = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("Data Deleted Successfully")) {
                            Toast.makeText(Info.this, "Data Deleted Successfully", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), Home.class);
                            startActivity(intent);
                            Info.this.finish();
                        } else {
                            Toast.makeText(Info.this, "Error in Deleting Data", Toast.LENGTH_SHORT).show();
                        }
                        dialog12.dismiss();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog12.dismiss();
                        if (error instanceof TimeoutError){
                            Timeout();
                        }else if (error instanceof NoConnectionError){
                            offlineMODE();
                        }
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> postData = new HashMap<>();
                        postData.put("id",ID);
                        return postData;
                    }
                };
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(delslams);
                dialog12.setTitle("Deleting Slam");
                dialog12.setCancelable(false);
                dialog12.setMessage("Please Wait");
                dialog12.show();
            }
        });
        ivProfile.setEnabled(false);
        edFname.setEnabled(false);
        edLname.setEnabled(false);
        edBirthday.setEnabled(false);
        edEmail.setEnabled(false);
        edAge.setEnabled(false);
        edZodiac.setEnabled(false);
        edHobby.setEnabled(false);
        edAmbition.setEnabled(false);
        edLove.setEnabled(false);
        edFcrush.setEnabled(false);
        edFlove.setEnabled(false);
    }
    private void offlineMODE(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Info.this).create();
        alertDialog.setTitle("Offline");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    private void Timeout(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Info.this).create();
        alertDialog.setTitle("Connection Timeout");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

}
